chrome.extension.onMessage.addListener(function(message, sender) {
    if (message && message.type === 'showPageAction') {
        var tab = sender.tab;
        chrome.pageAction.show(tab.id);
        chrome.pageAction.setTitle({
            tabId: tab.id,
            title: "ツイート"
        });
        chrome.pageAction.onClicked.addListener(function(tab){
        	var headder		= typeof localStorage["header"] == 'undefined'?"見ている：":localStorage["header"];
        	var footer		= typeof localStorage["footer"] == 'undefined'?"":localStorage["footer"];
        
        	var URL="https://twitter.com/intent/tweet?text="+encodeURIComponent(headder+tab.title+" "+tab.url+" "+footer);
        	window.open(URL,'windowname','width=700,height=483');
        });
    }
});