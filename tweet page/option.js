$(function(){

    var headder		= typeof localStorage["header"] == 'undefined'?"見ている：":localStorage["header"];
	$("#header").val(headder);
	$("#header").change(function(){
		var val = $(this).val();
		localStorage["header"] = val;
	});
	$("#footer").val(localStorage["footer"]);
	$("#footer").change(function(){
		var val = $(this).val();
		localStorage["footer"] = val;
	});
});